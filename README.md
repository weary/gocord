# Gocord

A fast and scalable Discord library for Go.

### Installation
```
go get github.com/gocord/gocord
```

### Support
Join the [Discord](https://discord.gg/apQh77XKAs) server for support.
