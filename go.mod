module github.com/gocord/gocord

go 1.16

require (
	github.com/gobwas/httphead v0.1.0 // indirect
	github.com/gobwas/pool v0.2.1 // indirect
	github.com/gobwas/ws v1.0.4
	github.com/valyala/fasthttp v1.28.0
)
