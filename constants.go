package gocord

import "errors"

var ErrClientCreate = errors.New("an error occurred while instantiating the client")

var ErrConnClose = errors.New("server issued connection close frame")

var ErrNoGateway = errors.New("failed to get gateway uri")
var ErrConnFailed = errors.New("failed to connect to gateway")
var ErrCannotRead = errors.New("failed to read gateway message")

var ErrDecompressEvent = errors.New("failed to decompress event message")
var ErrEventDecode = errors.New("failed to decode event message")
var ErrHeartbeat = errors.New("failed to send heartbeat")
var ErrExpectedHello = errors.New("gateway returned non-hello response")

var Intents = struct {
	GUILDS                   int
	GUILD_MEMBERS            int
	GUILD_BANS               int
	GUILD_EMOJIS             int
	GUILD_INTEGRATIONS       int
	GUILD_WEBHOOKS           int
	GUILD_INVITES            int
	GUILD_VOICE_STATES       int
	GUILD_PRESENCES          int
	GUILD_MESSAGES           int
	GUILD_MESSAGE_REACTIONS  int
	GUILD_MESSAGE_TYPING     int
	DIRECT_MESSAGES          int
	DIRECT_MESSAGE_REACTIONS int
	DIRECT_MESSAGE_TYPING    int
	ALL                      int
}{
	1 << 0, 1 << 1, 1 << 2,
	1 << 3, 1 << 4, 1 << 5,
	1 << 6, 1 << 7, 1 << 8,
	1 << 9, 1 << 10, 1 << 11,
	1 << 12, 1 << 13, 1 << 14,
	1 << 0 & 1 << 1 & 1 << 2 & 1 << 3 & 1 << 4 & 1 << 5 & 1 << 6 & 1 << 7 & 1 << 8 & 1 << 9 & 1 << 10 & 1 << 11 & 1 << 12 & 1 << 13 & 1 << 14,
}

var EVENTS = struct {
	READY               string
	MESSAGE_CREATE      string
	CHANNEL_CREATE      string
	CHANNEL_UPDATE      string
	CHANNEL_DELETE      string
	GUILD_BAN_ADD       string
	GUILD_BAN_REMOVE    string
	GUILD_MEMBER_ADD    string
	GUILD_MEMBER_REMOVE string
	GUILD_MEMBER_UPDATE string
}{
	"READY",
	"MESSAGE_CREATE",
	"CHANNEL_CREATE",
	"CHANNEL_UPDATE",
	"CHANNEL_DELETE",
	"GUILD_BAN_ADD",
	"GUILD_BAN_REMOVE",
	"GUILD_MEMBER_ADD",
	"GUILD_MEMBER_REMOVE",
	"GUILD_MEMBER_UPDATE",
}

var protectedEvents = map[string]bool{
	getEventName(EVENTS.READY):               true,
	getEventName(EVENTS.MESSAGE_CREATE):      true,
	getEventName(EVENTS.CHANNEL_CREATE):      true,
	getEventName(EVENTS.CHANNEL_UPDATE):      true,
	getEventName(EVENTS.CHANNEL_DELETE):      true,
	getEventName(EVENTS.GUILD_BAN_ADD):       true,
	getEventName(EVENTS.GUILD_BAN_REMOVE):    true,
	getEventName(EVENTS.GUILD_MEMBER_ADD):    true,
	getEventName(EVENTS.GUILD_MEMBER_REMOVE): true,
	getEventName(EVENTS.GUILD_MEMBER_UPDATE): true,
}
